#include "MainWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    //setWindowFlags(Qt::FramelessWindowHint);
    ui->setupUi(this);

    mDownloadManager = new QDownloadManager(this);
    mDownloadManager->addDownload("BMImage", "http://placekitten.com/g/200/150");
    connect(mDownloadManager, SIGNAL(downloadFinished(QString)), this, SLOT(on_downloadFinished(QString)));
    //New-QT5: connect(mDownloadManager, &QDownloadManager::downloadFinished, this, &MainWindow::on_downloadFinished);

    mBMSearchPage = 1;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_downloadFinished(QString name)
{
    qDebug() << "on_downloadFinished() received: " << name;
    if(!mDownloadManager->dataAvailable(name))
        return;

    //TODO Change those "BMImage" identifiers for an enum or something else
    if(name == "BMImage")
    {
        QPixmap pixmap;
        pixmap.loadFromData(mDownloadManager->retrieveDownloadedData(name));
        ui->lBackground->setPixmap(pixmap);
    }
    else if(name == "BMNewSearch")
    {
        QJsonDocument JSONDocument = QJsonDocument::fromJson(mDownloadManager->retrieveDownloadedData(name));
        if(!JSONDocument.isObject())
            return; //error
        updateBeatmapView(JSONDocument.object().value("maplist").toArray(), true);
    }
    else if(name == "BMAddSearch")
    {
        QJsonDocument JSONDocument = QJsonDocument::fromJson(mDownloadManager->retrieveDownloadedData(name));
        if(!JSONDocument.isObject())
            return; //error
        updateBeatmapView(JSONDocument.object().value("maplist").toArray());
    }
    else if(name.left(5) == "MAPDL")
    {
        QString fileName = name.split('|').last().append(".osz");
        QFile file(QDir::currentPath() + QStringLiteral("/") + fileName);
        qDebug() << QDir::currentPath() + QStringLiteral("/") + fileName;
        if(!file.open(QIODevice::WriteOnly))
            return;
        file.write(mDownloadManager->retrieveDownloadedData(name));
        qDebug() << "Writing data";
    }
}

void MainWindow::on_bSearch_clicked()
{
    if(ui->eSearch->text().isEmpty())
        return;

    int mBMSearchPage = 1;

    //TODO Add selectable beatmap provider
    QString searchUrl;
    searchUrl.append(QStringLiteral("http://"));
    searchUrl.append(QStringLiteral("loli.al"));
    searchUrl.append(QStringLiteral("/mirror/search/"));
    searchUrl.append(QString::number(mBMSearchPage));
    searchUrl.append(QStringLiteral(".json"));
    searchUrl.append(QStringLiteral("?keyword="));
    searchUrl.append(ui->eSearch->text().toUtf8());

    qDebug() << "Loading beatmap list from:" << searchUrl;

    mDownloadManager->addDownload("BMNewSearch", searchUrl);
}

void MainWindow::updateBeatmapView(QJsonArray BeatmapList, bool clear)
{
    if(BeatmapList.isEmpty())
        return;

    if(!clear)
        foreach (QJsonValue Beatmap, BeatmapList) {
            mBeatmapList.append(Beatmap);
        }
    else
        mBeatmapList = BeatmapList;

    //qDebug() << mBeatmapList;

    qDebug() << "Updating Beatmap view";

    ui->listWidget->clear();
    disconnect(ui->listWidget->verticalScrollBar(), 0, this, 0);

    foreach (QJsonValue BeatmapValue, mBeatmapList) {
        QJsonObject Beatmap = BeatmapValue.toObject();
        QListWidgetItem *item = new QListWidgetItem(Beatmap.value("Title").toString(tr("ERROR")));
        item->setData(Qt::UserRole, Beatmap);
        ui->listWidget->addItem(item);
    }

    connect(ui->listWidget->verticalScrollBar(), SIGNAL(valueChanged(int)), SLOT(on_scrollPositionChanged(int)));
}

void MainWindow::on_listWidget_currentItemChanged()
{
    if(!ui->listWidget->currentItem())
        return;
    QString selected = ui->listWidget->currentItem()->text();
    qDebug() << "Selected:" << selected;
    QJsonObject Beatmap = ui->listWidget->currentItem()->data(Qt::UserRole).toJsonObject();
    mDownloadManager->addDownload("BMImage", Beatmap.value("Background").toString(), 250);
    ui->lETitle->setText(Beatmap.value("Title").toString());
    ui->lESource->setText(Beatmap.value("Source").toString());
    ui->lEArtist->setText(Beatmap.value("Artist").toString());
    ui->lEMapper->setText(Beatmap.value("Mapper").toString());
    ui->lEDownload->setText(QStringLiteral("<a href=\"http://loli.al/") + Beatmap.value("Url").toString() + QStringLiteral("\">") + Beatmap.value("Url").toString() + QStringLiteral("<\a>"));
}

void MainWindow::on_scrollPositionChanged(int value)
{
    int max = ui->listWidget->verticalScrollBar()->maximum();
    //qDebug() << mDownloadManager->isRunning(QStringLiteral("BMAddSearch"));
    if(max - value <= 7 && !mDownloadManager->isRunning(QStringLiteral("BMAddSearch")))
    {
        QString searchUrl;
        searchUrl.append(QStringLiteral("http://"));
        searchUrl.append(QStringLiteral("loli.al"));
        searchUrl.append(QStringLiteral("/mirror/search/"));
        searchUrl.append(QString::number(++mBMSearchPage));
        searchUrl.append(QStringLiteral(".json"));
        searchUrl.append(QStringLiteral("?keyword="));
        searchUrl.append(ui->eSearch->text().toUtf8());

        qDebug() << "Adding beatmap list from:" << searchUrl;

        mDownloadManager->addDownload(QStringLiteral("BMAddSearch"), searchUrl, 100);
    }
}

void MainWindow::on_bDownloadSelected_clicked()
{
    int i = 1;
    foreach (QListWidgetItem *item, ui->listWidget->selectedItems()) {
        QJsonObject Beatmap = item->data(Qt::UserRole).toJsonObject();
        mDownloadManager->addDownload(QStringLiteral("MAPDL|") + QString::number(i++), QUrl(QStringLiteral("http://loli.al/") + Beatmap.value("Url").toString()), true);
    }
}
