#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QScrollBar>
#include <QListWidget>
#include <QFile>

#include <QDownloadManager/Core.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_downloadFinished(QString name);

    void on_bSearch_clicked();
    void on_listWidget_currentItemChanged();

    void on_scrollPositionChanged(int value);

    void on_bDownloadSelected_clicked();

private:
    //TODO Support more beatmap list for each beatmap provider's api
    void updateBeatmapView(QJsonArray BeatmapList/*, int format = 0*/, bool clear = false);

    Ui::MainWindow *ui;
    QDownloadManager *mDownloadManager;

    QJsonArray mBeatmapList;
    int mBMSearchPage;
};

#endif // MAINWINDOW_H
