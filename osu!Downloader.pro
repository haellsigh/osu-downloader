#-------------------------------------------------
#
# Project created by QtCreator 2015-03-25T22:11:36
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET =    osu!Downloader
TEMPLATE =  app


SOURCES +=  main.cpp\
            MainWindow.cpp

HEADERS  += MainWindow.h

FORMS    += MainWindow.ui

include(QDownloadManager/QDownloadManager.pri)
